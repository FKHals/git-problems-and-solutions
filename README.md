# Git Problems and Solutions

**Disclaimer**: Since `--force` is considered harmful ([see](https://blog.developer.atlassian.com/force-with-lease/)), one may try `--force-with-lease` instead. Although that might not work all the time, since many of these solutions heavily change the repository, so `--force` may be the only option in some cases.

## Table of Contents:
<!-- Generated using:
 grep '###' README.md | sed -E 's/^### ([^\n\r\(]+)(.*)/* [\1](#\1)/' | sed -E 's/\((.*)"(.*)"(.*)\)/(\1\\"\2\\"\3)/g'
(This also remove any parenthesized part of the title and escapes " in the link)
-->
* [Use a default username and password for a local repository](#Use a default username and password for a local repository)
* [Error: "remote: GitLab: Author '<current_author>' is not a member of team"](#Error: \"remote: GitLab: Author '<current_author>' is not a member of team\")
* [Modify a specific commit](#Modify a specific commit)
* [Add changes to a specific commit](#Add changes to a specific commit)
* [Moving last x commits to new branch and revert master to the commit before x](#Moving last x commits to new branch and revert master to the commit before x)
* [Get commit diff](#Get commit diff)
* [Delete changes of last commit ](#Delete changes of last commit )
* [Apply commit from other repository](#Apply commit from other repository)
* [Change author/email of specific commits](#Change author/email of specific commits)
* [Retrospectively change commit email ](#Retrospectively change commit email )
* [View git history of files or folders](#View git history of files or folders)
* [Squash last commits together ](#Squash last commits together )
* [Remove big binary blobs which bloat the repo ](#Remove big binary blobs which bloat the repo )
* [Ignore every file without an extension](#Ignore every file without an extension)
* [Pull into forked from original Repository](#Pull into forked from original Repository)
* [Use Vim as the default editor](#Use Vim as the default editor)
* [Fast-forward merges only on pull ](#Fast-forward merges only on pull )
* [Stage only parts of a changed file](#Stage only parts of a changed file)


-----

### Use a default username and password for a local repository

```shell
git config credential.helper store
git pull
```

-----

### Error: "remote: GitLab: Author '<current_author>' is not a member of team"

```shell
git config --global user.name "<new_name>"
git config --global user.email <example@mail.com>
git commit --amend --reset-author --no-edit
```
omit `--global` if it needs to be project-specific

-----

### Modify a specific commit

Solution for the example commit 'bbc643cd' (the '^' points to the commit BEFORE that commit since you actually need to rebase on that one):
```shell
git rebase --interactive 'bbc643cd^'
```
Then modify `pick` to `edit` in the line mentioning 'bbc643cd'.
Make your changes to the file <changed file> and then:
```shell
git add <changed file>
git commit --amend --no-edit
git rebase --continue
```
This alters the history, so `git push --force` may be needed after that

-----

### Add changes to a specific commit

Solution for the example commit with the ID 'OLDCOMMIT' ([source](https://stackoverflow.com/questions/2719579/how-to-add-a-changed-file-to-an-older-not-last-commit-in-git/27721031#27721031)):

Add the files you want to add to the specific (old) commit
```shell
git add <my fixed files>
```
Find the commit ID ('OLDCOMMIT') you want to add the changes to (e. g. with `git log --oneline`) and do:
```shell
git commit --fixup=OLDCOMMIT
git rebase --interactive --autosquash OLDCOMMIT^
```
(Do not forget the `^`! It points to the previous commit of the given commit (ID).)

Then just do `:wq` (if you use vim as the git editor) to accept the changes.

This alters the history, so `git push --force` may be needed after that

-----

### Moving last x commits to new branch and revert master to the commit before x

Create a new branch, containing all current commits
```shell
git branch newbranch
```
Move master back by 3 commits (Make sure you know how many commits you need to go back)
```shell
git reset --keep HEAD~3
```
Go to the new branch that still has the desired commits
```shell
git checkout newbranch
```

-----

### Get commit diff

COMMIT is the hash of a certain commit
```shell
git diff COMMIT~ COMMIT
```
Alternatively (for exactly one commit) use:
```shell
git show COMMIT
```

-----

### Delete changes of last commit (including the commit itself)

```shell
git reset --hard HEAD~1
```

-----

### Apply commit from other repository

add other repo with URL <url>
```shell
git remote add other_repo https://github.com/watabou/pixel-dungeon.git
git fetch other_repo
```
apply while in working directory 'pixel-dungeon-gdx/'
```shell
git --git-dir=../pixel-dungeon/.git format-patch -k -1 --stdout 66a1529002c94ffb5e22a25a5242dabd209c700c | git am --ignore-whitespace --ignore-space-change -3 -k --directory='core/'
```

-----

### Change author/email of specific commits

For example, if your commit history is A-B-C-D-E-F with F as HEAD, and you want to change the author of C and D, then you would:

```shell
git rebase -i C^
```
`C^` is the commit before C which is B. If you need to edit A, use `git rebase -i --root`.
Then change the lines for both C and D from pick to edit.
Once the rebase started, it would first pause at C, then you would do:
```shell
git commit --amend --no-edit --author="Author Name <email@address.com>"
git rebase --continue
```
It would pause again at D. Then you would repeat the previous step:
```shell
git commit --amend --no-edit --author="Author Name <email@address.com>"
git rebase --continue
```
The rebase would be complete and you can update your origin with the updated commits:
```shell
git push -f
```

-----

### Retrospectively change commit email (replace a certain email in all commits of a repo)

In my case i needed an email replacement to make my commit email private.

In GitLab for example to use a private email go to:

**Profile** > **Edit profile** > **Commit email** > **Use a private email** > **Update profile settings** ([source](https://gitlab.com/help/user/profile/index?target=_blank#private-commit-email))

To check the currently used global email:
```shell
git config --global user.email
```
1. So you may first change your global email to `<your_new_email_address>`:
(in my case it was <number>-<username>@users.noreply.gitlab.com i got from GitLab)
```shell
git config --global user.email <your_new_email_address>
```
2. Then `cd` into the repository you want to change and copy/paste the following script, replacing the variables `OLD_EMAIL`, `CORRECT_NAME`, `CORRECT_EMAIL` ([source](https://help.github.com/en/github/using-git/changing-author-info)):
```shell
#!/bin/sh

git filter-branch --env-filter '

OLD_EMAIL="your-old-email@example.com"
CORRECT_NAME="Your Correct Name"
CORRECT_EMAIL="your-correct-email@example.com"

if [ "$GIT_COMMITTER_EMAIL" = "$OLD_EMAIL" ]
then
export GIT_COMMITTER_NAME="$CORRECT_NAME"
export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$OLD_EMAIL" ]
then
export GIT_AUTHOR_NAME="$CORRECT_NAME"
export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
```
3. run the script

4. review new Git history for errors

5. Push corrected history to the server
```shell
git push --force --tags origin 'refs/heads/*'
```
6. If you want to change other repositories too, go back to 2. else you are done.

-----

### View git history of files or folders

```shell
git log -- path/to/file
```
```shell
git log -- path/to/folder
git log -- path/to/folder/*
```

-----

### Squash last commits together (overwrite them by the next comit)

If your last commit (for N commits instead of 1, use `HEAD~N`) was not usable (e. g. an
intermediate state) and you want to get rid of the commit while not changing the files use:
```shell
git reset --soft HEAD~1
```
and then make a new commit for everything:
```shell
git commit
```

-----

### Remove big binary blobs which bloat the repo (especially old ones which have been replaced)

This is especially important when you replace old binary blobs, like example files etc. that
are not needed any longer. Since git keeps all changes, it also keeps the changes of those
old files in the history. This is a way to remove that bloat and make your repo small(er) again!

1. `cd` into the repository you want to change and copy/paste the following script ([source](https://stackoverflow.com/questions/11255802/after-deleting-a-binary-file-from-git-history-why-is-my-repository-still-large/11277470#11277470)):
```shell
#!/usr/bin/env bash
git filter-branch --index-filter 'git rm -r -q --cached --ignore-unmatch '$1'' --prune-empty --tag-name-filter cat -- --all

rm -rf .git/refs/original/
git reflog expire --expire=now --all
git gc --prune=now
git gc --aggressive --prune=now
```
2. run that script on the file in the repo that needs to vanish, e. g. `ExampleFolder/Example.file` (it is really important to also get the filepath right!):
```shell
./script.sh ExampleFolder/Example.file
```
3. do step 2. with as many files as you like until everything is cleaned up to your liking

4. review new Git history for errors

5. Push corrected history to the server (may need a `--force`)
```shell
git push --all
git push --tags
```

-----

### Ignore every file without an extension

If your `git status`-call get cluttered by binaries or other extensionless things, this might come in handy.

Add the following Code at the beginning of your `.gitignore`-file ([source](https://stackoverflow.com/questions/19023550/how-do-i-add-files-without-dots-in-them-all-extension-less-files-to-the-gitign/19023985#19023985))
```shell
# Ignore everything
*

# Recursively unignore folders
!/**/

# Unignore all files with extensions
!*.*
```
You can also use this to only show certain file extensions by replacing `!*.*` with `!*.certain_file_extension` and adding other unignore-cases.

-----

### Pull into forked from original Repository

If you forked a repository (to the directory `FORKED_REPO`) and you want to update your repository with new commits from the original repo (`ORIGINAL_REPO_URL`, e.g. `https://gitlab.com/FKHals/git-problems-and-solutions.git`).

```shell
cd FORKED_REPO
git remote add upstream ORIGINAL_REPOSITORY_URL
git pull upstream master
```

-----

### Use Vim as the default editor

Since the new default editor is `nano`: If you prefer `vim` just do this to set it as the default git editor:
```shell
git config --global core.editor vim
```

-----

### Fast-forward merges only on pull (or: Stuff regarding `git pull`)

This replaces every `git pull` with `git pull --ff-only` ([source](https://stackoverflow.com/questions/15316601/in-what-cases-could-git-pull-be-harmful/15316602#15316602) and also [here](https://stackoverflow.com/questions/15316601/in-what-cases-could-git-pull-be-harmful/15316602#15316602)).
It stops `git pull` from creating merge commits when the history does not perfectly match.
In my opinion it is superior to be able to choose what to do (e. g. to to a rebase instead) then to merge by default.
```shell
git config --global pull.ff only
```
If a `git pull` command fails (triggered by that option) then you could first have a look at the things that changed without changing the local branch and merge/rebase afterwards:
```shell
git remote update -p  # or `git fetch --all -p`
```
If you already know that some commits have changed you can also do a pull-rebase:
```shell
git pull --rebase
```
To rebase on pull and also automatically stash uncommited changes beforehand and unstash afterwards by default use ([source](https://leosiddle.com/posts/2020/07/git-config-pull-rebase-autostash/#git-pull-rebase-vs-merge)):
```shell
git config --global pull.rebase true
# Alternatively used together with:
git config --global rebase.autoStash true
```
But especially the `autoStash` settings should be used with care: the final stash application after a successful rebase might result in non-trivial conflicts (see [git-rebase](https://git-scm.com/docs/git-rebase)).

-----

### Stage only parts of a changed file

Git supports interactive staging for that purpose (see [source](https://stackoverflow.com/questions/1085162/commit-only-part-of-a-file-in-git/1085191#1085191)).

To interactively stage parts of a certain file <filename> use (`-p` can also be used instead of `--patch`):
```shell
git add --patch <filename>
```
This iterates through sensible "hunks" of changes that can be individually staged (`y`) or not (`n`) or even split into smaller hunks using `s` (use `?` to show hunk help).

It is also possible to iterate over all hunks of all changed files instead of just one by omiting the filename:
```shell
git add --patch
```
To check if the parts of the changes have been staged as desired just use:
```shell
git diff --staged
```
Further reading: [git add --patch and --interactive](https://nuclearsquid.com/writings/git-add/)

